from django.shortcuts import render, get_object_or_404, redirect
from todos.models import TodoList, TodoItem
from todos.forms import TodoListForm, TodoItemForm


# Create your views here.
def todo_list_list(request):
    todos = TodoList.objects.all()
    context = {
        "todos": todos
    }

    return render(request, "todos/list.html", context)


def todo_list_detail(request, id):
    todo_list = get_object_or_404(TodoList, id=id)
    items = todo_list.items.all
    context = {
        "todo_list": todo_list,
        "items": items
    }
    return render(request, "todos/detail.html", context)


def create_todo_list(request):
    if request.method == "POST":
        form = TodoListForm(request.POST)
        if form.is_valid():
            form.save()
            list = form.save()

            return redirect("todo_list_detail", id=list.id)

    else:
        form = TodoListForm()

    context = {
            "form": form
        }
    return render(request, "todos/create.html", context)


def update_todo_list(request, id):
    todo_list = get_object_or_404(TodoList, id=id)
    if request.method == "POST":
        form = TodoListForm(request.POST, instance=todo_list)
        if form.is_valid():
            form.save()
            list = form.save()

            return redirect("todo_list_detail", id=list.id)

    else:
        # show existing prepopulated form
        form = TodoListForm(instance=todo_list)

    context = {
            "form": form
        }
    return render(request, "todos/update.html", context)


def delete_to_do_list(request, id):
    # create instance of model to get current object
    todo_list = get_object_or_404(TodoList, id=id)
    if request.method == "POST":
        # delete the object
        todo_list.delete()
        return redirect("todo_list_list")

    else:
        # show are you sure you want to delete? page
        context = {
            "todo_list": todo_list
        }
        return render(request, "todos/delete.html", context)


def create_todo_item(request):
    if request.method == "POST":
        form = TodoItemForm(request.POST)
        if form.is_valid():
            item = form.save()
            return redirect("todo_list_detail", id=item.list.id)

    else:
        form = TodoItemForm()
        context = {
            "form": form
        }
        return render(request, "todos/create_item.html", context)


def update_todo_item(request, id):
    todo_item = get_object_or_404(TodoItem, id=id)
    if request.method == "POST":
        form = TodoItemForm(request.POST, instance=todo_item)
        if form.is_valid():
            form.save()
            item = form.save()

            return redirect("todo_list_detail", id=item.list.id)

    else:
        form = TodoItemForm(instance=todo_item)
        context = {
            "form": form
        }
        return render(request, "todos/edit_item.html", context)
